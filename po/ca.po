# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2015. #zanata
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-04 16:42+0000\n"
"PO-Revision-Date: 2016-10-28 02:01-0400\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>\n"
"Language-Team: Catalan\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.9.6\n"

#: ../osinfo/osinfo_avatar_format.c:109
msgid "The allowed mime-types for the avatar"
msgstr "Els mime-types permesos per a l'avatar"

#: ../osinfo/osinfo_avatar_format.c:123
msgid "The required width (in pixels) of the avatar"
msgstr "L'amplada requerida (en píxels) de l'avatar"

#: ../osinfo/osinfo_avatar_format.c:140
msgid "The required height (in pixels) of the avatar."
msgstr "L'alçada requerida (en píxels) de l'avatar."

#: ../osinfo/osinfo_avatar_format.c:157
msgid "Whether alpha channel is supported in the avatar."
msgstr "Si permet el canal alfa en l'avatar."

#: ../osinfo/osinfo_deployment.c:154
msgid "Operating system"
msgstr "Sistema operatiu"

#: ../osinfo/osinfo_deployment.c:169
msgid "Virtualization platform"
msgstr "Plataforma de virtualització"

#: ../osinfo/osinfo_devicelink.c:129
msgid "Target device"
msgstr "Dispositiu de destinació"

#: ../osinfo/osinfo_devicelinkfilter.c:134
msgid "Device link target filter"
msgstr "Filtre de la destinació de l'enllaç al dispositiu"

#: ../osinfo/osinfo_entity.c:136
msgid "Unique identifier"
msgstr "Identificador únic"

#: ../osinfo/osinfo_install_config_param.c:152
msgid "Parameter name"
msgstr "Nom del paràmetre"

#: ../osinfo/osinfo_install_config_param.c:168
msgid "Parameter policy"
msgstr "Política del paràmetre"

#: ../osinfo/osinfo_install_config_param.c:184
msgid "Parameter Value Mapping"
msgstr "Assignació del valor del paràmetre"

#: ../osinfo/osinfo_install_script.c:192
msgid "URI for install script template"
msgstr "URI per a la plantilla del guió d'instal·lació"

#: ../osinfo/osinfo_install_script.c:204
msgid "Data for install script template"
msgstr "Dades per a la plantilla del guió d'instal·lació"

#: ../osinfo/osinfo_install_script.c:216
msgid "Install script profile name"
msgstr "Nom del perfil del guió d'instal·lació"

#: ../osinfo/osinfo_install_script.c:228
msgid "Product key format mask"
msgstr "Format de la màscara de la clau del producte"

#: ../osinfo/osinfo_install_script.c:238
msgid "Expected path format"
msgstr "Format esperat del camí"

#: ../osinfo/osinfo_install_script.c:249
msgid "Expected avatar format"
msgstr "Format esperat de l'avatar"

#: ../osinfo/osinfo_install_script.c:585
msgid "Unable to create XML parser context"
msgstr "No es pot crear el context de l'analitzador sintàctic XML"

#: ../osinfo/osinfo_install_script.c:593
msgid "Unable to read XSL template"
msgstr "No es pot llegir la plantilla XSL"

#: ../osinfo/osinfo_install_script.c:599
msgid "Unable to parse XSL template"
msgstr "No es pot crear analitzar sintàcticament la plantilla XSL"

#: ../osinfo/osinfo_install_script.c:672 ../osinfo/osinfo_install_script.c:708
#, c-format
msgid "Unable to create XML node '%s': '%s'"
msgstr "No es pot crear el node XML «%s»: «%s»"

#: ../osinfo/osinfo_install_script.c:680
#, c-format
msgid "Unable to create XML node 'id': '%s'"
msgstr "No es pot crear el 'id' del node XML: «%s»"

#: ../osinfo/osinfo_install_script.c:686 ../osinfo/osinfo_install_script.c:714
#, c-format
msgid "Unable to add XML child '%s'"
msgstr "No es pot afegir el fill XML «%s»"

#: ../osinfo/osinfo_install_script.c:761 ../osinfo/osinfo_install_script.c:772
#: ../osinfo/osinfo_install_script.c:796
#, c-format
msgid "Unable to set XML root '%s'"
msgstr "No es pot establir l'arrel XML «%s»"

#: ../osinfo/osinfo_install_script.c:784
#, c-format
msgid "Unable to set 'media' node: '%s'"
msgstr "No es pot establir el node «media»: «%s»"

#: ../osinfo/osinfo_install_script.c:819
msgid "Unable to create XSL transform context"
msgstr "No es pot crear el context de transformació XSL"

#: ../osinfo/osinfo_install_script.c:824
msgid "Unable to apply XSL transform context"
msgstr "No es pot aplicar el context de transformació XSL"

#: ../osinfo/osinfo_install_script.c:829
msgid "Unable to convert XSL output to string"
msgstr "No es pot convertir la sortida del XSL a una cadena de text"

#: ../osinfo/osinfo_install_script.c:889
#, c-format
msgid "Failed to load script template %s: "
msgstr "Error en carregar la plantilla de guió %s:"

#: ../osinfo/osinfo_install_script.c:904
#, c-format
msgid "Failed to apply script template %s: "
msgstr "Error en aplicar la plantilla de guió %s:"

#: ../osinfo/osinfo_install_script.c:958 ../osinfo/osinfo_install_script.c:1600
#: ../osinfo/osinfo_install_script.c:1648
msgid "Failed to apply script template: "
msgstr "Error en aplicar la plantilla de guió:"

#: ../osinfo/osinfo_list.c:132
msgid "List element type"
msgstr "Llista els tipus d'elements"

#: ../osinfo/osinfo_loader.c:195
#, c-format
msgid "Expected a nodeset in XPath query %s"
msgstr "S'esperava un nodeset en la consulta XPath %s"

#: ../osinfo/osinfo_loader.c:275 ../osinfo/osinfo_loader.c:420
msgid "Expected a text node attribute value"
msgstr "S'esperava un text en el valor de l'atribut del node"

#: ../osinfo/osinfo_loader.c:574
msgid "Missing device id property"
msgstr "Falta l'identificador de la propietat device"

#: ../osinfo/osinfo_loader.c:609
msgid "Missing device link id property"
msgstr "Falta l'identificador de la propietat device link"

#: ../osinfo/osinfo_loader.c:659
msgid "Missing product upgrades id property"
msgstr "Falta l'identificador de la propietat product upgrades"

#: ../osinfo/osinfo_loader.c:732
msgid "Missing platform id property"
msgstr "Falta l'identificador de la propietat platform"

#: ../osinfo/osinfo_loader.c:766
msgid "Missing deployment id property"
msgstr "Falta l'identificador de la propietat deployment"

#: ../osinfo/osinfo_loader.c:776
msgid "Missing deployment os id property"
msgstr "Falta l'identificador de la propietat deployment os"

#: ../osinfo/osinfo_loader.c:786
msgid "Missing deployment platform id property"
msgstr "Falta l'identificador de la propietat deployment platform"

#: ../osinfo/osinfo_loader.c:825 ../osinfo/osinfo_loader.c:1411
msgid "Missing os id property"
msgstr "Falta l'identificador de la propietat os"

#: ../osinfo/osinfo_loader.c:949
msgid "Missing install script id property"
msgstr "Falta l'identificador de la propietat install script"

#: ../osinfo/osinfo_loader.c:1526
msgid "Missing OS install script property"
msgstr "Falta l'identificador de la propietat OS install script"

#: ../osinfo/osinfo_loader.c:1590
msgid "Incorrect root element"
msgstr "Element root incorrecte"

#: ../osinfo/osinfo_loader.c:1657
msgid "Unable to construct parser context"
msgstr "No es pot construir el context de l'analitzador sintàctic"

#: ../osinfo/osinfo_loader.c:1679
msgid "Missing root XML element"
msgstr "Falta l'element XML root"

#: ../osinfo/osinfo_loader.c:2294
#, c-format
msgid ""
"$OSINFO_DATA_DIR is deprecated, please use $OSINFO_SYSTEM_DIR instead. "
"Support for $OSINFO_DATA_DIR will be removed in a future release\n"
msgstr ""

#: ../osinfo/osinfo_loader.c:2330 ../osinfo/osinfo_loader.c:2362
#, c-format
msgid ""
"%s is deprecated, please use %s instead. Support for %s will be removed in a "
"future release\n"
msgstr ""
"%s està en desús, utilitzeu %s en lloc seu. La compatibilitat per a %s serà "
"eliminada en un futur llançament\n"

#: ../osinfo/osinfo_media.c:391 ../osinfo/osinfo_resources.c:170
#: ../osinfo/osinfo_tree.c:273
msgid "CPU Architecture"
msgstr "Arquitectura de la CPU"

#: ../osinfo/osinfo_media.c:404
msgid "The URL to this media"
msgstr "L'URL a aquest mitjà"

#: ../osinfo/osinfo_media.c:417 ../osinfo/osinfo_tree.c:299
msgid "The expected ISO9660 volume ID"
msgstr "L'identificador ISO9660 esperat del volum"

#: ../osinfo/osinfo_media.c:430 ../osinfo/osinfo_tree.c:312
msgid "The expected ISO9660 publisher ID"
msgstr "L'identificador ISO9660 esperat de l'editor"

#: ../osinfo/osinfo_media.c:443 ../osinfo/osinfo_tree.c:325
msgid "The expected ISO9660 application ID"
msgstr "L'identificador ISO9660 esperat de l'aplicació"

#: ../osinfo/osinfo_media.c:456 ../osinfo/osinfo_tree.c:338
msgid "The expected ISO9660 system ID"
msgstr "L'identificador ISO9660 esperat del sistema"

#: ../osinfo/osinfo_media.c:469 ../osinfo/osinfo_tree.c:351
msgid "The path to the kernel image"
msgstr "El camí a la imatge del nucli del sistema operatiu"

#: ../osinfo/osinfo_media.c:482
msgid "The path to the initrd image"
msgstr "El camí a la imatge initrd"

#: ../osinfo/osinfo_media.c:495
msgid "Media provides an installer"
msgstr "El mitjà proporciona una eina d'instal·lació"

#: ../osinfo/osinfo_media.c:508
msgid "Media can boot directly w/o installation"
msgstr "El mitjà pot arrencar sense la instal·lació"

#: ../osinfo/osinfo_media.c:530
msgid "Number of installer reboots"
msgstr "Número de re-arrencades de l'eina d'instal·lació"

#: ../osinfo/osinfo_media.c:548
msgid "Information about the operating system on this media"
msgstr "Informació quant al sistema operatiu en aquest mitjà"

#: ../osinfo/osinfo_media.c:570
msgid "Supported languages"
msgstr "Idiomes suportats"

#: ../osinfo/osinfo_media.c:582
msgid "Expected ISO9660 volume size, in bytes"
msgstr "S'esperava la mida del volum ISO9660, en bytes"

#: ../osinfo/osinfo_media.c:602
msgid "Whether the media should be ejected after the installtion process"
msgstr ""

#: ../osinfo/osinfo_media.c:719
msgid "Failed to read supplementary volume descriptor: "
msgstr "Ha fallat la lectura del descriptor suplementari del volum:"

#: ../osinfo/osinfo_media.c:726
#, c-format
msgid "Supplementary volume descriptor was truncated"
msgstr "El descriptor secundari del volum estava truncat"

#: ../osinfo/osinfo_media.c:750
#, c-format
msgid "Install media is not bootable"
msgstr "El mitjà d'instal·lació no es pot arrencar"

#: ../osinfo/osinfo_media.c:812
msgid "Failed to read primary volume descriptor: "
msgstr "Ha fallat la lectura del descriptor primari del volum:"

#: ../osinfo/osinfo_media.c:819
#, c-format
msgid "Primary volume descriptor was truncated"
msgstr "El descriptor primari del volum estava truncat"

#: ../osinfo/osinfo_media.c:851
#, c-format
msgid "Insufficient metadata on installation media"
msgstr "Metadades insuficients en el mitjà d'instal·lació"

#: ../osinfo/osinfo_media.c:885
#, c-format
msgid "Failed to skip %d bytes"
msgstr "Error en ometre %d bytes"

#: ../osinfo/osinfo_media.c:890
#, c-format
msgid "No volume descriptors"
msgstr "Sense descriptors de volum"

#: ../osinfo/osinfo_media.c:921
msgid "Failed to open file"
msgstr "Error en obrir el fitxer"

#: ../osinfo/osinfo_os.c:151
msgid "Generic Family"
msgstr "Família genèrica"

#: ../osinfo/osinfo_os.c:167
msgid "Generic Distro"
msgstr "Distribució genèrica"

#: ../osinfo/osinfo_product.c:169 ../tools/osinfo-query.c:59
#: ../tools/osinfo-query.c:83 ../tools/osinfo-query.c:109
msgid "Name"
msgstr "Nom"

#: ../osinfo/osinfo_product.c:182 ../tools/osinfo-query.c:57
#: ../tools/osinfo-query.c:81
msgid "Short ID"
msgstr "Id. curt"

#: ../osinfo/osinfo_product.c:195 ../tools/osinfo-query.c:67
#: ../tools/osinfo-query.c:87 ../tools/osinfo-query.c:101
msgid "Vendor"
msgstr "Proveïdor"

#: ../osinfo/osinfo_product.c:208 ../tools/osinfo-query.c:61
#: ../tools/osinfo-query.c:85
msgid "Version"
msgstr "Versió"

#: ../osinfo/osinfo_product.c:221
msgid "Codename"
msgstr "Nom clau"

#: ../osinfo/osinfo_product.c:234
msgid "URI of the logo"
msgstr "URI del logotip"

#: ../osinfo/osinfo_resources.c:186
msgid "CPU frequency in hertz (Hz)"
msgstr "Freqüència de la CPU amb hertz (Hz)"

#: ../osinfo/osinfo_resources.c:203
msgid "Number of CPUs"
msgstr "Número de CPU"

#: ../osinfo/osinfo_resources.c:220
msgid "Amount of Random Access Memory (RAM) in bytes"
msgstr "Quantitat de RAM (Random Access Memory) en bytes"

#: ../osinfo/osinfo_resources.c:237
msgid "Amount of storage space in bytes"
msgstr "Quantitat d'espai d'emmagatzematge en bytes"

#: ../osinfo/osinfo_tree.c:286
msgid "The URL to this tree"
msgstr "La URI a aquest arbre"

#: ../osinfo/osinfo_tree.c:364
msgid "The path to the inirtd image"
msgstr "El camí a la imatge de l'inirtd"

#: ../osinfo/osinfo_tree.c:377
msgid "The path to the bootable ISO image"
msgstr "El camí a la imatge ISO d'arrencada"

#: ../osinfo/osinfo_tree.c:601
msgid "Failed to load .treeinfo file: "
msgstr "Ha fallat la càrrega del fitxer .treeinfo: "

#: ../osinfo/osinfo_tree.c:611
msgid "Failed to process keyinfo file: "
msgstr "Ha fallat el processament del fitxer keyinfo: "

#: ../osinfo/osinfo_os_variant.c:115
msgid "The name to this variant"
msgstr "El nom d'aquesta variant"

#: ../tools/osinfo-detect.c:65 ../tools/osinfo-detect.c:86
#, c-format
msgid "Invalid value '%s'"
msgstr "El valor no és vàlid «%s»"

#: ../tools/osinfo-detect.c:98
msgid "Output format. Default: plain"
msgstr "Format de sortida. Per defecte: plain"

#: ../tools/osinfo-detect.c:99
msgid "plain|env."
msgstr "plain|env."

#: ../tools/osinfo-detect.c:102
msgid "URL type. Default: media"
msgstr "Tipus d'URL. Per defecte: media"

#: ../tools/osinfo-detect.c:103
msgid "media|tree."
msgstr "media|tree."

#: ../tools/osinfo-detect.c:113
#, c-format
msgid "Media is bootable.\n"
msgstr "El mitjà és d'arrencada.\n"

#: ../tools/osinfo-detect.c:118
#, c-format
msgid "Media is not bootable.\n"
msgstr "El mitjà no és d'arrencada.\n"

#: ../tools/osinfo-detect.c:155
#, c-format
msgid "Media is an installer for OS '%s'\n"
msgstr "El mitjà és un instal·lador per al SO «%s»\n"

#: ../tools/osinfo-detect.c:157
#, c-format
msgid "Media is live media for OS '%s'\n"
msgstr "El mitjà és un mitjà autònom per al SO «%s»\n"

#: ../tools/osinfo-detect.c:162
#, c-format
msgid "Available OS variants on media:\n"
msgstr "Variants disponibles del SO en el mitjà:\n"

#: ../tools/osinfo-detect.c:207
#, c-format
msgid "Tree is an installer for OS '%s'\n"
msgstr "L'arbre és un instal·lador per al SO «%s»\n"

#: ../tools/osinfo-detect.c:224
msgid "- Detect if media is bootable and the relevant OS and distribution."
msgstr ""
"- Detecta si el sistema operatiu es d'arrencada, el rellevant del SO i la "
"distribució."

#: ../tools/osinfo-detect.c:228 ../tools/osinfo-query.c:415
#, c-format
msgid "Error while parsing commandline options: %s\n"
msgstr ""
"Error en analitzar sintàcticament les opcions de la línia d'ordres: %s\n"

#: ../tools/osinfo-detect.c:246 ../tools/osinfo-install-script.c:342
#: ../tools/osinfo-query.c:431
#, c-format
msgid "Error loading OS data: %s\n"
msgstr "Error en carregar les dades del SO: %s\n"

#: ../tools/osinfo-detect.c:262
#, c-format
msgid "Error parsing media: %s\n"
msgstr "Error en analitzar sintàcticament el mitjà: %s\n"

#: ../tools/osinfo-detect.c:280
#, c-format
msgid "Error parsing installer tree: %s\n"
msgstr "Error en analitzar sintàcticament l'arbre de l'instal·lador: %s\n"

#: ../tools/osinfo-install-script.c:53
#, c-format
msgid "Expected configuration key=value"
msgstr "Configuració esperada clau=valor"

#: ../tools/osinfo-install-script.c:71
msgid "Install script profile"
msgstr "Perfil del guió d'instal·lació"

#: ../tools/osinfo-install-script.c:73
msgid "Install script output directory"
msgstr "Directori de sortida del guió d'instal·lació"

#: ../tools/osinfo-install-script.c:75
msgid "The output filename prefix"
msgstr "El prefix del nom de fitxer de sortida"

#: ../tools/osinfo-install-script.c:78
msgid "Set configuration parameter"
msgstr "Estableix el paràmetre de configuració"

#: ../tools/osinfo-install-script.c:80
msgid "List configuration parameters"
msgstr "Llista els paràmetres de configuració"

#: ../tools/osinfo-install-script.c:82
msgid "List install script profiles"
msgstr "Llista els perfils del guió d'instal·lació"

#: ../tools/osinfo-install-script.c:84
msgid "List supported injection methods"
msgstr "Llista els mètodes d'injecció suportats"

#: ../tools/osinfo-install-script.c:86
msgid "Do not display output filenames"
msgstr "No mostris els noms dels fitxers de sortida"

#: ../tools/osinfo-install-script.c:143
#, c-format
msgid "No install script for profile '%s' and OS '%s'"
msgstr "Sense guió d'instal·lació per al perfil «%s» i el SO «%s»"

#: ../tools/osinfo-install-script.c:159
msgid "required"
msgstr "obligatori"

#: ../tools/osinfo-install-script.c:159
msgid "optional"
msgstr "opcional"

#: ../tools/osinfo-install-script.c:245
#, c-format
msgid "No install script for profile '%s' and OS '%s'\n"
msgstr "Cap script d'instal·lació per al perfil «%s» i el SO «%s»\n"

#: ../tools/osinfo-install-script.c:274
#, c-format
msgid "Unable to generate install script: %s\n"
msgstr "No es pot generar el guió d'instal·lació: %s\n"

#: ../tools/osinfo-install-script.c:311
msgid "- Generate an OS install script"
msgstr "- Genera un guió d'instal·lació del SO"

#: ../tools/osinfo-install-script.c:314
#, c-format
msgid "Error while parsing options: %s\n"
msgstr ""
"S'ha produït un error mentre s'analitzaven sintàcticament les opcions: %s\n"

#: ../tools/osinfo-install-script.c:332
msgid ""
"Only one of --list-profile, --list-config and --list-injection-methods can "
"be requested"
msgstr ""
"Únicament es pot sol·licitar un dels següents--list-profile, --list-config o "
"--list-injection-methods"

#: ../tools/osinfo-install-script.c:361
#, c-format
msgid "Error finding OS: %s\n"
msgstr "Error en trobar el SO: %s\n"

#: ../tools/osinfo-query.c:63
msgid "Family"
msgstr "Família"

#: ../tools/osinfo-query.c:65
msgid "Distro"
msgstr "Distribució"

#: ../tools/osinfo-query.c:69 ../tools/osinfo-query.c:89
msgid "Release date"
msgstr "Data del llançament"

#: ../tools/osinfo-query.c:71 ../tools/osinfo-query.c:91
msgid "End of life"
msgstr "Fi del suport"

#: ../tools/osinfo-query.c:73 ../tools/osinfo-query.c:93
msgid "Code name"
msgstr "Nom clau"

#: ../tools/osinfo-query.c:75 ../tools/osinfo-query.c:95
#: ../tools/osinfo-query.c:115 ../tools/osinfo-query.c:121
msgid "ID"
msgstr "Identificador"

#: ../tools/osinfo-query.c:103
msgid "Vendor ID"
msgstr "Id. del proveïdor"

#: ../tools/osinfo-query.c:105
msgid "Product"
msgstr "Producte"

#: ../tools/osinfo-query.c:107
msgid "Product ID"
msgstr "Id. del producte"

#: ../tools/osinfo-query.c:111
msgid "Class"
msgstr "Classe"

#: ../tools/osinfo-query.c:113
msgid "Bus"
msgstr "Bus"

#: ../tools/osinfo-query.c:153 ../tools/osinfo-query.c:191
#, c-format
msgid "Unknown property name %s"
msgstr "Nom desconegut de propietat %s"

#: ../tools/osinfo-query.c:177
msgid "Syntax error in condition, expecting KEY=VALUE"
msgstr "Error sintàctic en la condició, s'espera CLAU=VALOR"

#: ../tools/osinfo-query.c:403
msgid "Sort column"
msgstr "Ordena la columna"

#: ../tools/osinfo-query.c:405
msgid "Display fields"
msgstr "Mostra els camps"

#: ../tools/osinfo-query.c:410
msgid "- Query the OS info database"
msgstr "- Consulta el SO en la base de dades"

#: ../tools/osinfo-query.c:422
#, c-format
msgid "Missing data type parameter\n"
msgstr "Falta el paràmetre de tipus de dades\n"

#: ../tools/osinfo-query.c:450
#, c-format
msgid "Unknown type '%s' requested\n"
msgstr "S'ha sol·licitat un tipus desconegut «%s»\n"

#: ../tools/osinfo-query.c:455
#, c-format
msgid "Unable to construct filter: %s\n"
msgstr "No es pot construir el filtre: %s\n"

#: ../tools/osinfo-query.c:460
#, c-format
msgid "Unable to set field visibility: %s\n"
msgstr "No es pot establir la visibilitat del camp: %s\n"
